
<?
include("confiq.php")

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Paket Data</title>
   <link rel="stylesheet" href="style.css" type="text/css" media="all" />
   <style type="text/css" media="all">
     .bg-slide{
  width: 100%;
  height: auto;
}
.bg-slide form{
  width: 100%;
  height: auto;
}
.bg-slide label{
  margin-left: 7%;
  font-size: 1rem;
  margin-top: 1rem;
  position: absolute;
}
.bg-slide input {
  width: 90%;
  height: auto;
  padding: 2%;
  border: none;
  font-size: 1.2rem;
  margin-left: 5%;
  margin-top: 13%;
  border-bottom: 1px solid rgb(173,173,173);
  outline: none;
}
.bg-slide h1{
  font-size: 0.8rem;
  position: absolute;
  font-weight: normal;
  margin-left: 20rem;
  margin-top: -1.8rem;
  opacity: 0.8;
  background-color: rgb(41,38,38);
  color: white;
  border-radius: 50px;
  padding-left: 1%;
  padding-right: 1%;
}
.bg-slide span{
  font-size: 0.6rem;
  margin-left: 6%;
  color: red;
  display: none;
}

.btn-bg{
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 5rem;
  text-align: center;
  padding-top: 1.1rem;
}
.btn-bg button{
  width: 90%;
  height: auto;
  font-size: 1.2rem;
  border-radius: 50px;
  padding: 2%;
  border: none;
  background-color: rgb(191,191,191);
  color: white;
  outline: none;
}
#clear{
  display: none;
}

/* menu pulsa Desain */
.banner-img{
  width: 100%;
  height: auto;
}
.banner-img img{
  width: 100%;
  height: auto;
}

.bg-header{
  width: 100%;
  height: auto;
  border-bottom: 1px solid rgb(190,190,190);
}
.header{
  width: 100%;
  height: auto;
}
.header form{
  width: 100%;
  height: auto;
  margin-top: 5%;
  margin-bottom: 5%;
}
.header label{
  font-size: 0.7rem;
  margin-left: 10%;
}

.header input{
  width: 80%;
  height: auto;
  padding: 2%;
  margin-left: 10%;
  outline: none;
  border-radius: 50px;
  border: 1px solid rgb(190,190,190);
  padding-left: 5%;
}
.produk-bg{
  width: 100%;
  height: 40rem;
  background-color: #f5f5f5;
}
.container-produk{
  width: 45%;
  height: 6rem;
  background-color: white;
  margin-left: 2.5%;
  margin-right: 2.5%;
  float: left;
  margin-bottom: 2.5%;
  margin-top: 2.5%;
  border-radius: 10px;
  padding: 3%;
}
.produk{
  width: 100%;
  height: 100%;
}
.produk h1{
  font-size: 1.2rem;
}
.produk p{
  font-size: 0.8rem;
  margin-bottom: 0;
}
.produk h2{
  font-size: 1rem;
  color: #26a69a;
}
    .tinggi-konten{
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
}
#offcanvasBottomLabel{
  font-size: 16px;
  text-align: center;
  margin-left: 25%;
  opacity: 0.6;
  margin-top: -2%;
}
.jarakKlik{
  width: 100%;
  min-height: 7px;
  background-color: black;
  opacity: 0.090;
}
.dataKlik{
  width: 102%;
  height: 200%;
  background-color: white;
  position: absolute;
  margin-left: -5%;
  margin-top: -5%;
  padding-top: 3%;
}
.tinggi-konten{
  margin-top: -90%;
  height: auto;
  background-color: white;
}
.dataKlik h2{
  margin-left: 5%;
  font-size: 14px;
  opacity: 0.8;
  float: left;
}
.dataKlik h3{
  margin-left: 5%;
  font-size: 17px;
  opacity: 0.8;
}
.dataKlik h5{
  font-size: 14px;
  margin-left: 5%;
  float: left;
  border: 1px solid #26A795;
  padding: 4px;
  border-radius: 5px;
  margin-right: 0.5%;
  margin-top: 2%;
  margin-bottom: 5%;
  font-weight: normal;
  padding-left: 10px;
  padding-right: 10px;
  color: #26A795;
}
.dataKlik h5:hover{
  background-color: #26A795;
  color: white;
}

.dataKlik p{
  margin-left: 5%;
  font-size: 14px;
  opacity: 0.6;
  text-align: right;
  margin-right: 6%;
}
.cleaning{
  position: absolute;
  border: none;
  border-radius: 50px;
  color: white;
  background-color: rgb(137,137,137);
  margin-top: -27rem;
  padding-left: 3%;
  padding-right: 3%;
  font-size: 1rem;
  margin-left: 20rem;
  outline: none;
}
.cleaning:hover{
  background-color: orange;
}
.postdata{
  margin-top: 2%;
  width: 80%;
  float: left;
  margin-left: 10%;
  border: none;
  background-color: #26A795;
  color: white;
  padding-top: 7px;
  padding-bottom: 7px;
  text-align: center;
  border-radius: 50px;
  padding: 3%;
  outline: none;
}
.bg-post{
  width: 80%;
  height: 5.5rem;
  background-color: #26a69a;
  border-radius: 10px;
  margin-top: 5%;
  margin-bottom: 5%;
  margin-left: 10%;
  padding: 3%;
}
.bg-post td{
  font-size: 0.8rem;
  color: white;
}
#popupNonAktiv{
  display: none;
}
.popUpKesalahan{
  width: 100%;
  height: auto;
  background-color: rgba(0,0,0,0.448);
  top: 0;
  left: 0;
  bottom: 0;
  position: absolute;
  position: fixed;
}
.card-show{
  width: 80%;
  margin-left: 10%;
  height: 22rem;
  margin-top: 30%;
  margin-bottom: 100%;
  border-radius: 5px;
  background-color: white;
  text-align: center;
}
.card-show button{
  width: 90%;
  font-size: 1.2rem;
  background-color: #26a69a;
  color: white;
  border: none;
  border-radius: 50px;
  padding: 3%;
  outline: none;
}

.card-show button:hover{
  background-color: orange;
}
.card-show img{
  width: 100%;
  height: auto;
  border-radius: 5px;
}
   </style>
  </head>
  <body>
 <section id="linkUrl">
    <div class="banner-img">
      <img src="https://layanan.pln.co.id:8060/files/img_Terangi_Negeri-Web_Banner_W_475_x_H_253px_20201012104743.jpg" alt="" />
    </div>
      <?php

      $hasil = json_decode($response, TRUE);

      $res = $hasil["data"]["res"];
      $id = $hasil["data"]["meter_number"];
      $tarif = $hasil["data"]["segmentation"];
      $daya = $hasil["data"]["power"];

      ?>
      
<section class="bg-header">
  <div class="header">
    <form action="" method="get" accept-charset="utf-8">
      <label for="">NO METER / ID PELANGGAN</label>
      <input oninput="cekValue()" type="number" name="" id="inputUser" value="<?php echo $id_pel; ?>" readonly>
      <div class="bg-post">
        <table>
          <tr>
              <td>Nickname</td>
              <td>:</td>
              <td id="namaPelanggan"><b><?php echo $res; ?></b></td>
          </tr>
          <tr>
              <td>ID Pelanggan</td>
              <td>:</td>
              <td id="nomorIdPelanggan"><b><?php echo $id_pel; ?></b></td>
          </tr>
      </table>
      </div>
    </form>
  </div>
</section>


<section class="produk-bg">
  <div  class="container-produk" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" onclick="kirimTransaksi1()">
    <div class="produk">
      <h1 id="namaBarang1"> 20.000</h1>
      <p>Harga</p>
      <h2 id="hargaBarang1">Rp.20.500</h2>
      <a id="urlBarang1" href="https://venuss.my.id/produk/pln-20-000-708379"></a>
    </div>
  </div>
<div  class="container-produk" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" onclick="kirimTransaksi2()">
    <div class="produk">
      <h1 id="namaBarang2"> 50.000</h1>
      <p>Harga</p>
      <h2 id="hargaBarang2">Rp.50.500</h2>
      <a id="urlBarang2" href="https://venuss.my.id/produk/pln-50-000-708380"></a>
    </div>
  </div>
  <div  class="container-produk" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" onclick="kirimTransaksi3()">
    <div class="produk">
      <h1 id="namaBarang3"> 100.000</h1>
      <p>Harga</p>
      <h2 id="hargaBarang3">Rp.100.500</h2>
      <a id="urlBarang3" href="https://venuss.my.id/produk/pln-100-000-708381"></a>
    </div>
  </div>
  <div  class="container-produk" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" onclick="kirimTransaksi4()">
    <div class="produk">
      <h1 id="namaBarang4"> 200.000</h1>
      <p>Harga</p>
      <h2 id="hargaBarang4">Rp.200.500</h2>
      <a id="urlBarang4" href="https://venuss.my.id/produk/pln-200-000-708382"></a>
    </div>
  </div>
  <div  class="container-produk" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" onclick="kirimTransaksi5()">
    <div class="produk">
      <h1 id="namaBarang5"> 500.000</h1>
      <p>Harga</p>
      <h2 id="hargaBarang5">Rp.500.500</h2>
      <a id="urlBarang5" href="https://venuss.my.id/produk/pln-500-000-708383"></a>
    </div>
  </div>
  <div  class="container-produk" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" onclick="kirimTransaksi6()">
    <div class="produk">
      <h1 id="namaBarang6"> 1.000.000</h1>
      <p>Harga</p>
      <h2 id="hargaBarang6">Rp.1.000.500</h2>
      <a id="urlBarang6" href="https://venuss.my.id/produk/pln-1-000-000-708384"></a>
    </div>
  </div>
</section>
</section>




<section id="popupNonAktiv" class="popUpKesalahan">
  <div class="card-show">
    <img src="https://work.e-saku.id/imge/errorP.jpg" alt="" />
    <button onclick="kembaliHome()" type="submit">Oke</button>
  </div>
</section>


  <!-- AREA DATA SAAT DIKLIK OLEH USER --> 
   <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel" style="border-top-left-radius: 20px; border-top-right-radius: 20px;">
  <div class="offcanvas-header tinggi-konten">
    <!-- Konten header canvas !-->
    <h5 class="offcanvas-title" id="offcanvasBottomLabel">
      konfirmasi Pembelian
    </h5>
  </div>
  <div class="offcanvas-body medium">
    <div class="dataKlik">
    <h2>Nama Pelanggan:</h2>
    <p id="namaTujuan">none</p>
    <h2>Nomor Meter:</h2>
    <p id="noIdTujuan">none</p>
    <h2>Nominal:</h2>
    <p id="jumlahBarang" style="font-weight:bold;">none</p>
    <h2>Tarif / daya:</h2>
    <p id="dayaTujuan">none</p>
    <div style="clear:both;">
    </div>
     <hr class="jarakKlik"/>
    <h3>Detail pembayaran:</h3>
    <h2>Harga:</h2>
    <p id="HargaBarang">none</p>
    <h2>Biaya Admin:</h2>
    <p id="biayaLainya">Rp2.500</p>
    <h2>Diskon & Potongan:</h2>
    <p id="metodePembayaran">Rp0</p>
    <p id="linkUrl"></p>
    <div style="border-bottom:1px dashed rgb(197,197,197);">
    </div>
    
    
    <h2 style="color: black; margin-top: 4%; font-size: 16px; margin-left:4%; opacity:1;">Total Pembayaran:</h2>
    
    <p id="totalTagihan" style="color: black; margin-top: 4%; font-size: 16px; opacity:1;">none</p>
    
    <button class="cleaning" type="button" data-bs-dismiss="offcanvas" aria-label="Close">x</button>
    
    <button type="button" class="postdata" onclick="kirimData()">Lanjutkan</button>
    </div>
  </div>
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <script src="js.js" type="text/javascript" charset="utf-8"></script>
  <script src="meta.js" type="text/javascript"></script>
  <script src="core.js" type="text/javascript"></script>
  </body>
</html>