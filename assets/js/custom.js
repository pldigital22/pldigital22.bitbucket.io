(function ($) {

	"use strict";

	// Page Ready

	// Welcome Area background image
	if ($('.welcome-area').length) {
		$('.welcome-area .welcome-bg').css("background-image", "url(" + $('.welcome-area .welcome-bg').data('bg') + ")");
	}


	// Menu Dropdown Toggle
	if ($('.menu-trigger').length) {
		$(".menu-trigger").on('click', function () {
			$(this).toggleClass('active');
			$('.header-area .nav').slideToggle(200);
		});
	}


	// Scroll animation init
	window.sr = new scrollReveal();


	// Home number counterup
	if ($('.count-item').length) {
		$('.count-item strong').counterUp({
			delay: 10,
			time: 1000
		});
	}


	// App single gallery
	if ($('.gallery').length && $('.gallery-item').length) {
		$('.gallery-item').magnificPopup({
			type: 'image',
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				duration: 300,
				easing: 'ease-in-out',
			}
		});
	}


	// Page standard gallery
	if ($('.page-gallery').length && $('.page-gallery-wrapper').length) {
		$('.page-gallery').imgfix({
			scale: 1.1
		});

		$('.page-gallery').magnificPopup({
			type: 'image',
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				duration: 300,
				easing: 'ease-in-out',
			}
		});
	}


	// About Us Image
	if ($('.about-image').length) {
		$('.about-image').imgfix({
			scale: 1.1
		});
	}


	// Blog cover image
	if ($('.blog-post-thumb').length) {
		$('.blog-post-thumb .img').imgfix();
	}


	// Services page cover image
	if ($('.services-post').length) {
		$('.services-post .img').imgfix();
	}


	// Page loading animation
	$(window).on('load', function () {
		if ($('.cover').length) {
			$('.cover').parallax({
				imageSrc: $('.cover').data('image'),
				zIndex: '1'
			});
		}

		$(".loader-wrapper").animate({
			'opacity': '0'
		}, 600, function () {
			setTimeout(function () {
				// Parallax init
				if ($('.parallax').length) {
					$('.parallax').parallax({
						imageSrc: 'assets/images/photos/parallax.jpg',
						zIndex: '1'
					});
				}
				$(".loader-wrapper").css("visibility", "hidden").fadeOut();
			}, 300);
		});
	});



	// Header Scrolling Set White Background
	$(window).on('scroll', function () {
		var width = $(window).width();
		if ($('.header-white').length) {
			return false;
		} else {
			if (width > 991) {
				var scroll = $(window).scrollTop();
				if (scroll >= 30) {
					$(".header-area").addClass("header-sticky");
					$(".header-area .dark-logo").css('display', 'block');
					$(".header-area .light-logo").css('display', 'none');
				} else {
					$(".header-area").removeClass("header-sticky");
					$(".header-area .dark-logo").css('display', 'none');
					$(".header-area .light-logo").css('display', 'block');
				}
			}
		}
	});


})(window.jQuery);




$(document).on('click', '.send_form', function () {
	var input_blanter = document.getElementById('wa_email');

	/* Whatsapp Settings */
	var walink = 'https://web.whatsapp.com/send',
		phone = '6281414567089',
		walink2 = 'Halo saya ingin Deposit ',
		text_yes = 'Terkirim.',
		text_no = 'Isi semua Formulir lalu klik Send.';

	/* Smartphone Support */
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		var walink = 'whatsapp://send';
	}

	if ("" != input_blanter.value) {

		/* Call Input Form */
		var input_select1 = $("#wa_select :selected").text(),
			input_name1 = $("#wa_name").val(),
			input_email1 = $("#wa_email").val(),
			input_number1 = $("#wa_number").val(),
			input_number2 = $("#wa_deposit").val(),

		/* Final Whatsapp URL */
		var blanter_whatsapp = walink + '?phone=' + phone + '&text=' + walink2 + '%0A%0A' +
			'*Name* : ' + input_name1 + '%0A' +
			'*Email Address* : ' + input_email1 + '%0A' +
			'*Select Option* : ' + input_select1 + '%0A' +
			'*Input Number* : ' + input_number1 + '%0A' +
			'*Input Number* : ' + input_number2 + '%0A' +

			/* Whatsapp Window Open */
			window.open(blanter_whatsapp, '_blank');
		document.getElementById("text-info").innerHTML = '<span class="yes">' + text_yes + '</span>';
	} else {
		document.getElementById("text-info").innerHTML = '<span class="no">' + text_no + '</span>';
	}
});