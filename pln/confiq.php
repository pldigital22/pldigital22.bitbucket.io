

<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
error_reporting(0);

function get_token(){
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://www.bukalapak.com/auth_proxies/request_token?client_id=&client_secret=&grant_type=&scope=",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "accept: application/vnd.bukalapak.v4+json",
        "accept-encoding: gzip, deflate, br",
        "accept-language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7,ms;q=0.6",
        "alexatoolbar-alx_ns_ph: AlexaToolbar/alx-4.0.3",
        "authority: www.bukalapak.com",
        "cache-control: no-cache,no-cache",
        "content-type: multipart/form-data; boundary=----WebKitFormBoundaryoeawnVn5AqgUbmDZ",
        "cookie: identity=; browser_id=;",
        "origin: https://www.bukalapak.com",
        "pragma: no-cache",
        "referer: https://www.bukalapak.com/",
        "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36"
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    return $response;
}

$tokens = get_token();

$tokens = json_decode($tokens, TRUE);

$token = $tokens["access_token"];

$idpln = $_POST["regex"];

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.bukalapak.com/electricities/prepaid-inquiries?access_token=$token",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"customer_number\":\"$idpln\",\"product_id\":134}",
  CURLOPT_HTTPHEADER => array(
    "Accept: application/json, text/plain, */*",
    "Accept-Encoding: gzip, deflate, br",
    "Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7,ms;q=0.6",
    "AlexaToolbar-ALX_NS_PH: AlexaToolbar/alx-4.0.3",
    "Cache-Control: no-cache",
    "Connection: keep-alive",
    "Content-Type: application/json;charset=UTF-8",
    "Origin: https://www.bukalapak.com",
    "Pragma: no-cache",
    "Referer: https://www.bukalapak.com/listrik-pln/token-listrik",
    "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);


// jika nomor meter tersedia tampilkan link berikut ini
$link = "/v1/pln/plnMenu.php";
// jika nomor meter tidak tersedia alihakan user ke halaman awal
$linkA = "";
?>



