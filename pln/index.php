<?php
include("confiq.php")
?>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <title>Token PLN</title>
  <link rel="stylesheet" href="style.css" type="text/css" media="all" />
  <style type="text/css" media="all">
    .bg-slide {
      width: 100%;
      height: auto;
    }

    .bg-slide form {
      width: 100%;
      height: auto;
    }

    .bg-slide label {
      margin-left: 7%;
      font-size: 1rem;
      margin-top: 1rem;
      position: absolute;
    }

    .bg-slide input {
      width: 90%;
      height: auto;
      padding: 2%;
      border: none;
      font-size: 1.2rem;
      margin-left: 5%;
      margin-top: 13%;
      border-bottom: 1px solid rgb(173, 173, 173);
      outline: none;
    }

    .bg-slide h1 {
      font-size: 0.8rem;
      position: absolute;
      font-weight: normal;
      margin-left: 20rem;
      margin-top: -1.8rem;
      opacity: 0.8;
      background-color: rgb(41, 38, 38);
      color: white;
      border-radius: 50px;
      padding-left: 1%;
      padding-right: 1%;
    }

    .bg-slide span {
      font-size: 0.6rem;
      margin-left: 6%;
      color: red;
      display: none;
    }

    .btn-bg {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 5rem;
      text-align: center;
      padding-top: 1.1rem;
    }

    .btn-bg button {
      width: 90%;
      height: auto;
      font-size: 1.2rem;
      border-radius: 50px;
      padding: 2%;
      border: none;
      background-color: rgb(191, 191, 191);
      color: white;
      outline: none;
    }

    #clear {
      display: none;
    }

    /* menu pulsa Desain */
    .banner-img {
      width: 100%;
      height: auto;
    }

    .banner-img img {
      width: 100%;
      height: auto;
    }

    .bg-header {
      width: 100%;
      height: auto;
      border-bottom: 1px solid rgb(190, 190, 190);
    }

    .header {
      width: 100%;
      height: auto;
    }

    .header form {
      width: 100%;
      height: auto;
      margin-top: 5%;
      margin-bottom: 5%;
    }

    .header label {
      font-size: 0.7rem;
      margin-left: 10%;
    }

    .header input {
      width: 80%;
      height: auto;
      padding: 2%;
      margin-left: 10%;
      outline: none;
      border-radius: 50px;
      border: 1px solid rgb(190, 190, 190);
      padding-left: 5%;
    }

    .produk-bg {
      width: 100%;
      height: 40rem;
      background-color: #f5f5f5;
    }

    .container-produk {
      width: 45%;
      height: 6rem;
      background-color: white;
      margin-left: 2.5%;
      margin-right: 2.5%;
      float: left;
      margin-bottom: 2.5%;
      margin-top: 2.5%;
      border-radius: 10px;
      padding: 3%;
    }

    .produk {
      width: 100%;
      height: 100%;
    }

    .produk h1 {
      font-size: 1.2rem;
    }

    .produk p {
      font-size: 0.8rem;
      margin-bottom: 0;
    }

    .produk h2 {
      font-size: 1rem;
      color: #26a69a;
    }

    .tinggi-konten {
      border-top-left-radius: 10px;
      border-top-right-radius: 10px;
    }

    #offcanvasBottomLabel {
      font-size: 16px;
      text-align: center;
      margin-left: 25%;
      opacity: 0.6;
      margin-top: -2%;
    }

    .jarakKlik {
      width: 100%;
      min-height: 7px;
      background-color: black;
      opacity: 0.090;
    }

    .dataKlik {
      width: 102%;
      height: 200%;
      background-color: white;
      position: absolute;
      margin-left: -5%;
      margin-top: -5%;
      padding-top: 3%;
    }

    .tinggi-konten {
      margin-top: -90%;
      height: auto;
      background-color: white;
    }

    .dataKlik h2 {
      margin-left: 5%;
      font-size: 14px;
      opacity: 0.8;
      float: left;
    }

    .dataKlik h3 {
      margin-left: 5%;
      font-size: 17px;
      opacity: 0.8;
    }

    .dataKlik h5 {
      font-size: 14px;
      margin-left: 5%;
      float: left;
      border: 1px solid #26A795;
      padding: 4px;
      border-radius: 5px;
      margin-right: 0.5%;
      margin-top: 2%;
      margin-bottom: 5%;
      font-weight: normal;
      padding-left: 10px;
      padding-right: 10px;
      color: #26A795;
    }

    .dataKlik h5:hover {
      background-color: #26A795;
      color: white;
    }

    .dataKlik p {
      margin-left: 5%;
      font-size: 14px;
      opacity: 0.6;
      text-align: right;
      margin-right: 6%;
    }

    .cleaning {
      position: absolute;
      border: none;
      border-radius: 50px;
      color: white;
      background-color: rgb(137, 137, 137);
      margin-top: -27rem;
      padding-left: 3%;
      padding-right: 3%;
      font-size: 1rem;
      margin-left: 20rem;
      outline: none;
    }

    .cleaning:hover {
      background-color: orange;
    }

    .postdata {
      margin-top: 2%;
      width: 80%;
      float: left;
      margin-left: 10%;
      border: none;
      background-color: #26A795;
      color: white;
      padding-top: 7px;
      padding-bottom: 7px;
      text-align: center;
      border-radius: 50px;
      padding: 3%;
      outline: none;
    }

    .bg-post {
      width: 80%;
      height: 5.5rem;
      background-color: #26a69a;
      border-radius: 10px;
      margin-top: 5%;
      margin-bottom: 5%;
      margin-left: 10%;
      padding: 3%;
    }

    .bg-post td {
      font-size: 0.8rem;
      color: white;
    }

    #popupNonAktiv {
      display: none;
    }

    .popUpKesalahan {
      width: 100%;
      height: auto;
      background-color: rgba(0, 0, 0, 0.448);
      top: 0;
      left: 0;
      bottom: 0;
      position: absolute;
      position: fixed;
    }

    .card-show {
      width: 80%;
      margin-left: 10%;
      height: 22rem;
      margin-top: 30%;
      margin-bottom: 100%;
      border-radius: 5px;
      background-color: white;
      text-align: center;
    }

    .card-show button {
      width: 90%;
      font-size: 1.2rem;
      background-color: #26a69a;
      color: white;
      border: none;
      border-radius: 50px;
      padding: 3%;
      outline: none;
    }

    .card-show button:hover {
      background-color: orange;
    }

    .card-show img {
      width: 100%;
      height: auto;
      border-radius: 5px;
    }
  </style>
</head>

<body>
  <section class="bg-slide">
    <form action="<?php echo $link; ?>" method="post" accept-charset="utf-8">
      <label for="">ID Pelanggan</label>
      <input type="number" name="regex" id="inputID" value="" placeholder="Masukkan ID Pelanggan / No.Meteran "
        oninput="cekInput(clear)" />
      <h1 onclick="clearData()" id="clear">×</h1>
      <span id="alertWarning">Masukkan 5-15 karakter</span>
      <div class="btn-bg">
        <button id="btnBig" type="submit">Lanjutkan</button>
      </div>
    </form>
  </section>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
  </script>
  <script src="js.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript" charset="utf-8">
    let input = document.getElementById('inputID');

    document.getElementById('inputID').addEventListener('input', function (e) {
      e.target.value = e.target.value.replace(/[^0-9]/g, '').substring(0, 16).trim();
    });


    function cekInput(e) {
      let clear = document.getElementById('clear');
      let inputID = document.getElementById('inputID').value;
      let info = document.getElementById('alertWarning');
      let btn = document.getElementById('btnBig');

      // this function
      if (inputID.length > 5) {
        clear.style.display = "block";
        btn.style = "background-color:#26a69a; color: white;";
        input.style = "border-bottom: 1px solid #26a69a;";
        info.style.display = "none";
      } else if (inputID.length < 3) {
        clear.style.display = "none";
        btn.style = "background-color:rgb(191,191,191); color: white;";
        input.style = "border-bottom: 1px solid rgb(173,173,173);";
        info.style.display = "block";
      }
    }

    function clearData() {
      // body...
      let inputID = document.getElementById('inputID').value = "";
    }
  </script>
</body>

</html>